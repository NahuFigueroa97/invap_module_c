from pymakelib import module
from pathlib import Path

@module.ModuleClass
class TempConv(module.BasicCModule):
    
    def getSrcs(self) -> list:
        srcs =super().getSrcs()
        resp = []
        for src in srcs:
            if not 'test' in Path(src).parts:
                resp.append(src)
        return resp