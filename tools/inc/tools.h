#ifndef TOOLS_H
#define TOOLS_H

#include <stdlib.h>

/**
 * @brief Invert string
 * 
 * @param out Output string
 * @param len Length of output string
 * @param in  Input string
 */
void tools_invert_string(char *out,  size_t len, char* in);

/**
 * @brief Compare function
 */
typedef int (*CompareFunc)(float a, float b);

/**
 * @brief Sort arrays of floats
 * 
 * @param array  Input array
 * @param array_len  Length of array
 * @param fun Poiinter to compare function
 */
void tools_array_sort(float array[], int len, CompareFunc fun);


/**
 * @brief Return random element of integer array 
 * 
 * @param array Input array
 * @param len Length of array
 * @return int integer value
 */
int tools_rand_int(int array[], int len);


/**
 * @brief Return random element of array
 * 
 * @param array Input array
 * @param array Size of element
 * @param len Length of array
 * @return int integer value
 */
void* tools_rand_element(void* array, size_t elem_size, int len);


#endif // TOOLS_H
