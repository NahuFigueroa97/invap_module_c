#ifdef TEST

#include "unity.h"
#include <string.h>

#include "tools.h"

void setUp(void)
{
}

void tearDown(void)
{
}

void test_tools_invert_string(void)
{
    char *in = "Test1234567890";
    char out[32];
    memset(out, 0, sizeof(out));
    tools_invert_string(out, sizeof(out), in);
    TEST_ASSERT_EQUAL_STRING("0987654321tseT", out);
}

void test_tools_rand_element(void)
{
    int in[] = {1, 2, 3, 4, 5, 6, 7, 8};
    int value = tools_rand_int(in, 8);
    TEST_ASSERT_GREATER_OR_EQUAL_INT(1, value);
    TEST_ASSERT_LESS_OR_EQUAL_INT(8, value);
}

#endif // TEST
