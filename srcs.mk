####################################################
#    class_1/temp_conv/temp_conv_mk.py:TempConv    #
####################################################
CSRC += class_1/temp_conv/src/temp_conv.c

SRC_DIRS += class_1/temp_conv/src

INCS += -Iclass_1/temp_conv/inc



####################################################
#             tools/tools_mk.py:Tools              #
####################################################
CSRC += tools/src/tools.c

SRC_DIRS += tools/src

INCS += -Itools/inc



####################################################
#                app/app_mk.py:App                 #
####################################################
CSRC += app/src/main.c

SRC_DIRS += app/src

INCS += -Iapp/inc



