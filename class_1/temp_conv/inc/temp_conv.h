#ifndef TEMP_CONV_H
#define TEMP_CONV_H

#include <stdlib.h>

#define ROOM_TEMP 25.0f

enum _TempUnit {
    CELSIUS_U  = 1,
    FAHRENHEIT_U,
    KELVIN_U
};
typedef enum _TempUnit TempUnit; 

/**
 * @brief Temperature converter
 * temp_cover(CELSIUS_U, FAHRENHEIT_U, 25.0f)
 * 
 * @param in Input temperature unit 
 * @param out Output temperature unit
 * @param in_temp Input temperature value
 * @return float Temperata output
 */
float temp_conv(TempUnit in, TempUnit out, float in_temp);

/**
 * @brief Print list of temperatures in different units
 * temp_cover(out, 100, CELSIUS_U, 25.0f, FAHRENHEIT_U, 77.0f, KELVIN_U, 298.15f, NULL);
 * 
 * @param buffout Output buffer 
 * @param buff_size size of buffer
 * @param in Input temperature unit 
 * @param out Output temperature unit
 * @param var_args list of temperatures
 */
void temp_conv_snprint(char *buffout, size_t buff_size, ...);

#endif // TEMP_CONV_H
