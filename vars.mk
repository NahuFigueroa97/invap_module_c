PROJECT         = invap_module_c
PROJECT_OUT     = Release/Objects

CC         := /usr/bin/gcc
CXX        := /usr/bin/g++
LD         := /usr/bin/gcc
AR         := /usr/bin/ar
AS         := /usr/bin/as
OBJCOPY    := /usr/bin/objcopy
SIZE       := /usr/bin/size
OBJDUMP    := /usr/bin/objdump
NM         := /usr/bin/nm
RANLIB     := /usr/bin/ranlib
STRINGS    := /usr/bin/strings
STRIP      := /usr/bin/strip
CXXFILT    := /usr/bin/c++filt
ADDR2LINE  := /usr/bin/addr2line
READELF    := /usr/bin/readelf
ELFEDIT    := /usr/bin/elfedit

# MACROS
COMPILER_FLAGS += -DTEST
# MACHINE-OPTS
COMPILER_FLAGS += 
# OPTIMIZE-OPTS
COMPILER_FLAGS += 
# OPTIONS
COMPILER_FLAGS += 
# DEBUGGING-OPTS
COMPILER_FLAGS += -g3
# PREPROCESSOR-OPTS
COMPILER_FLAGS += -MP -MMD
# WARNINGS-OPTS
COMPILER_FLAGS += 
# CONTROL-C-OPTS
COMPILER_FLAGS += -std=gnu11
# GENERAL-OPTS
COMPILER_FLAGS += 
# LIBRARIES
COMPILER_FLAGS += 

# LINKER-SCRIPT
LDFLAGS += 
# MACHINE-OPTS
LDFLAGS += 
# GENERAL-OPTS
LDFLAGS += 
# LINKER-OPTS
LDFLAGS += 
# LIBRARIES
LDFLAGS += 
