TARGET          = Release/invap_module_c

TARGETS = $(TARGET)


$(TARGET): $(OBJECTS) $(SLIBS_OBJECTS) 
	$(call logger-compile,"OUT",$@)
	$(LD) -o $@ $(OBJECTS) $(LDFLAGS)


clean_targets:
	rm -rf $(TARGET)
